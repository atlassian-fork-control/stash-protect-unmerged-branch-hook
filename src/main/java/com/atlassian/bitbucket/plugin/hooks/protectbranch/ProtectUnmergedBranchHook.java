package com.atlassian.bitbucket.plugin.hooks.protectbranch;

import com.atlassian.bitbucket.hook.HookResponse;
import com.atlassian.bitbucket.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.pull.*;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.RefChangeType;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.StandardRefType;
import com.atlassian.bitbucket.scm.git.GitRefPattern;
import com.atlassian.bitbucket.util.*;
import com.google.common.collect.Iterables;

import javax.annotation.Nonnull;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Iterator;

import static java.util.stream.Collectors.toList;

/**
 * This hook disallows pushing a branch deletion when the branch is part of
 * an active (not merged/declined) branch.
 */
public class ProtectUnmergedBranchHook implements PreReceiveRepositoryHook {

    private static final String UNABLE_TO_DELETE_BRANCH_TEMPLATE_KEY = "bitbucket.plugins.hooks.protect.unmerged.branch.unable.to.delete.template";

    private final PullRequestService pullRequestService;
    private final I18nService i18nService;
    private final NavBuilder navBuilder;

    public ProtectUnmergedBranchHook(PullRequestService pullRequestService, I18nService i18nService, NavBuilder navBuilder) {
        this.pullRequestService = pullRequestService;
        this.i18nService = i18nService;
        this.navBuilder = navBuilder;
    }

    @Override
    public boolean onReceive(@Nonnull RepositoryHookContext context, @Nonnull Collection<RefChange> refChanges,
                             @Nonnull HookResponse hookResponse) {
        Iterable<RefChange> refDeletes = refChanges.stream()
                .filter(change -> change.getType().equals(RefChangeType.DELETE))
                .filter(change-> change.getRef().getType().equals(StandardRefType.BRANCH))
                .collect(toList());

        boolean hasActiveBranchDeletes = false;
        for (RefChange refDelete : refDeletes) {
            Iterator<PullRequest> pullRequests = listActivePullRequestsForBranch(context.getRepository(), refDelete.getRef().getId()).iterator();
            if (pullRequests.hasNext()) {
                if (!hasActiveBranchDeletes) {
                    hasActiveBranchDeletes = true;
                    printDashes(hookResponse);
                }
                formatActiveBranchDeletes(hookResponse.err(), refDelete.getRef().getId(), pullRequests);
            }
        }
        if (hasActiveBranchDeletes) {
            printDashes(hookResponse);
        }

        return !hasActiveBranchDeletes;
    }

    private void printDashes(HookResponse hookResponse) {
        hookResponse.err().println("-----------------------------------------------------");
    }

    private void formatActiveBranchDeletes(PrintWriter printWriter, String refId, Iterator<PullRequest> pullRequests) {
        String unqualifiedRefId = GitRefPattern.HEADS.unqualify(refId);
        printWriter.println(i18nService.getText(UNABLE_TO_DELETE_BRANCH_TEMPLATE_KEY,
                "Unable to delete branch ''{0}'' because it is involved in the following pull requests:", unqualifiedRefId));
        while (pullRequests.hasNext()) {
            PullRequest pullRequest = pullRequests.next();
            Repository targetRepository = pullRequest.getToRef().getRepository();
            //noinspection ConstantConditions
            printWriter.println("\t" + navBuilder.repo(targetRepository).pullRequest(pullRequest.getId()).buildAbsolute());
        }
    }

    private Iterable<PullRequest> listActivePullRequestsForBranch(Repository repository, String branchRefId) {
        return Iterables.concat(findActivePullRequestsInDirection(repository, branchRefId, PullRequestDirection.INCOMING),
                                findActivePullRequestsInDirection(repository, branchRefId, PullRequestDirection.OUTGOING));
    }

    private Iterable<PullRequest> findActivePullRequestsInDirection(Repository repository, String branchRefId,
                                                                    PullRequestDirection direction) {
        return new PagedIterable<>(pageRequest -> {
            PullRequestSearchRequest request = new PullRequestSearchRequest.Builder()
                    .repositoryAndBranch(direction, repository.getId(), branchRefId)
                    .state(PullRequestState.OPEN)
                    .build();
            return pullRequestService.search(request, pageRequest);
        }, new PageRequestImpl(0, 50));
    }
}
