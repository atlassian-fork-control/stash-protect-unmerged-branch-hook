package com.atlassian.bitbucket.plugin.hooks.protectbranch.util;

import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageUtils;

import java.util.Arrays;

/**
 * Some test util functions that get the PageRequest out of the way.  Most tests don't care.
 * If you care about the PageRequest size/start, use the PageUtils methods.
 */
public class TestPageUtils {
    public static <T> Page<T> pageContaining(T... elements) {
        if (elements.length == 0) {
            return emptyPage();
        } else {
            return PageUtils.createPage(Arrays.asList(elements), PageUtils.newRequest(0, elements.length));
        }
    }

    public static <T> Page<T> emptyPage() {
        return PageUtils.createEmptyPage(PageUtils.newRequest(0, 10));
    }
}
